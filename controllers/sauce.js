const fs = require("fs");
const Sauce = require("../models/sauce");

exports.createSauce = (req, res, next) => {
  //   delete req.body._id;
  const sauceObject = JSON.parse(req.body.sauce);
  delete sauceObject._id;
  const sauce = new Sauce({
    ...sauceObject,
    imageUrl: `${req.protocol}://${req.get("host")}/images/${
      req.file.filename
    }`,
  });
  sauce
    .save()
    .then(() => res.status(201).json({ message: "Sauce enregistrée !" }))
    .catch((error) => res.status(400).json({ error }));
};

exports.getAllSauces = (req, res, next) => {
  Sauce.find()
    .then((sauces) => res.status(200).json(sauces))
    .catch((error) => res.status(400).json({ error }));
};

exports.getSauce = (req, res, next) => {
  Sauce.findOne({ _id: req.params.id })
    .then((sauce) => res.status(200).json(sauce))
    .catch((error) => res.status(404).json({ error }));
};

exports.modifySauce = (req, res, next) => {
  const sauceObject = req.file
    ? {
        ...JSON.parse(req.body.sauce),
        imageUrl: `${req.protocol}://${req.get("host")}/images/${
          req.file.filename
        }`,
      }
    : { ...req.body };
  Sauce.updateOne(
    { _id: req.params.id },
    { ...sauceObject, _id: req.params.id }
  )
    .then(() => res.status(200).json({ message: "Sauce modifiée !" }))
    .catch((error) => res.status(400).json({ error }));
};

exports.deleteSauce = (req, res, next) => {
  Sauce.findOne({ _id: req.params.id })
    .then((sauce) => {
      const filename = sauce.imageUrl.split("/images/")[1];
      fs.unlink(`images/${filename}`, () => {
        Sauce.deleteOne({ _id: req.params.id })
          .then(() => res.status(200).json({ message: "Sauce supprimée !" }))
          .catch((error) => res.status(400).json({ error }));
      });
    })
    .catch((error) => res.status(500).json({ error }));
};

exports.likeSauce = (req, res, next) => {
  Sauce.findOne({ _id: req.params.id })
    .then((sauce) => {
      const userLike = req.body.like;
      const userId = req.body.userId;
      const usersLikedIndexOfuserId = sauce.usersLiked.indexOf(userId);
      const usersDislikedIndexOfuserId = sauce.usersDisliked.indexOf(userId);
      const liked = usersLikedIndexOfuserId >= 0 ? true : false;
      const disliked = usersDislikedIndexOfuserId >= 0 ? true : false;

      // Si like = 0, l'utilisateur annule son like ou son dislike
      if (userLike === 0 && (liked || disliked)) {
        if (liked) {
          sauce.usersLiked.splice(usersLikedIndexOfuserId, 1);
          sauce.likes = sauce.likes - 1;
        } else {
          sauce.usersDisliked.splice(usersDislikedIndexOfuserId, 1);
          sauce.dislikes = sauce.dislikes - 1;
        }
        sauce
          .save()
          .then(() =>
            res
              .status(200)
              .json({ message: "Like ou dislike supprimé ! (annulé)" })
          )
          .catch((error) => res.status(400).json({ error }));
      }
      // Si like = 1, l'utilisateur aime (= like) la sauce.
      if (userLike === 1) {
        sauce.usersLiked.push(userId);
        sauce.likes = sauce.likes + 1;
        sauce
          .save()
          .then(() => res.status(200).json({ message: "Sauce likée !" }))
          .catch((error) => res.status(400).json({ error }));
      }
      // Si like = -1, l'utilisateur n'aime pas (= dislike) la sauce.
      if (userLike === -1) {
        sauce.usersDisliked.push(userId);
        sauce.dislikes = sauce.dislikes + 1;
        sauce
          .save()
          .then(() => res.status(200).json({ message: "Sauce dislikée !" }))
          .catch((error) => res.status(400).json({ error }));
      }
    })
    .catch((error) => res.status(500).json({ error }));
};
